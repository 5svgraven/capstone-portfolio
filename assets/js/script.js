var btn = $("#btb");

$(window).scroll(function () {
  if ($(window).scrollTop() > 300) {
    btn.addClass("show");
  } else {
    btn.removeClass("show");
  }
});

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

function btnsubmit() {
  Swal.fire("Message Sent!", "Thank you for Messaging", "success");
}

$(document).ready(function () {
  var map = null;
  var myMarker;
  var myLatlng;

  function initializeGMap(lat, lng) {
    myLatlng = new google.maps.LatLng(lat, lng);

    var myOptions = {
      zoom: 12,
      zoomControl: true,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    myMarker = new google.maps.Marker({
      position: myLatlng,
    });
    myMarker.setMap(map);
  }

  // Re-init map before show modal
  $("#mapModal").on("show.bs.modal", function (event) {
    var button = $(event.relatedTarget);
    initializeGMap(button.data("lat"), button.data("lng"));
    $("#location-map").css("width", "100%");
    $("#map_canvas").css("width", "100%");
  });

  // Trigger map resize event after modal shown
  $("#mapModal").on("shown.bs.modal", function () {
    google.maps.event.trigger(map, "resize");
    map.setCenter(myLatlng);
  });
});

// Hide nav after click
$(".nav-link").on("click", function () {
  $(".navbar-collapse").collapse("hide");
});
